import {Component} from '@angular/core';
import {EditorService} from "./editor/editor.service";
import {Subscription} from "rxjs";
import {ChartDataSets} from "chart.js";
import {Color} from "ng2-charts";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'questionnaire';
    subscriptions: Subscription [] = [];
    question: string = '';
    options: string[] = [];
    chartData: ChartDataSets[];
    chartLabels: string[];
    chartColors: Color[] =  [{backgroundColor: ['#8567ff', "#a29bff", "#cbb5ff", "#E7E9ED", "#7d8fff", "#885ac0",
            "#9785ff", "#bbb5ed", "#b89eeb", "#8791ff"]}];

    constructor(public service: EditorService) {
    }

    ngOnInit(): void {
        this.subscribe = this.service.votesSubject.subscribe(() => {
            this.updateChartData(this.options, this.service.votes);
        });
    }

    public onQuestionChanged(value: string) {
        this.question = value;
    }

    public onOptionsChanged(options: string[]) {
        this.options = options;
        this.updateChartData(options, this.service.votes);
    }

    onReset(){
        this.service.reset();
    }
    private updateChartData(options, votes) {
        this.chartLabels = options;
        const data = options.map((label: string) => votes[label]);
        this.chartData = [{data: data, label: ''}];
    }

    set subscribe(subscription: Subscription) {
        this.subscriptions.push(subscription);
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }

    vote(key: string) {
        if (key) {
            this.service.vote = key;
        }
    }
}

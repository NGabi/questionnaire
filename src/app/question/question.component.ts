import {Component, Output, EventEmitter, Input, OnChanges, SimpleChanges} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
    selector: 'app-question',
    templateUrl: './question.component.html',
    styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnChanges{
    @Output() voteEvent = new EventEmitter<string>();
    @Input() question: string;
    @Input() options: string[] = [];
    questionForm = new FormGroup({'answer': new FormControl(null, Validators.required)});
    ngOnChanges(changes: SimpleChanges): void {
        if(changes.options && changes.options.previousValue.length >  changes.options.currentValue.length){
            this.questionForm.get('answer').patchValue(null);
        }
    }

    onSubmit(): void {
        const key = this.questionForm.value.answer;
        this.voteEvent.emit(key);
    }
}

import {Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class EditorService {
    votesSubject = new BehaviorSubject({});
    nrOfVotes = 0;
    reset() {
        this.votesSubject.next({});
        this.nrOfVotes = 0;
    }

    set vote(key: string) {
        const votes = this.votesSubject.getValue();
        votes[key] = votes[key] ? votes[key] + 1 : 1;
        this.votesSubject.next(votes);
        this.nrOfVotes++;
    }

    get votes() {
        return this.votesSubject.getValue();
    }
}

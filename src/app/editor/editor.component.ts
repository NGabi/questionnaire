import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {Subscription} from "rxjs";

@Component({
    selector: 'app-editor',
    templateUrl: './editor.component.html',
    styleUrls: ['./editor.component.scss']
})
export class EditorComponent implements OnInit {
    @Output() questionEvent = new EventEmitter<string>();
    @Output() optionsEvent = new EventEmitter<string[]>();
    @Output() resetEvent = new EventEmitter();
    subscriptions: Subscription [] = [];
    form = new FormGroup({
        'question': new FormControl(null, Validators.required),
        'addOption': new FormControl(null, Validators.required),
        'options': new FormArray([new FormControl('answer option 1'),
            new FormControl('answer option 2')])
    });

    ngOnInit(): void {
        this.optionsEvent.emit(['answer option 1', 'answer option 2']);
        this.subscribe = this.form.get('question').valueChanges.subscribe(value => {
            this.questionEvent.emit(value);
        });

        this.subscribe = this.form.get('options').valueChanges.subscribe(value => {
            this.optionsEvent.emit(value);
        });
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }

    onAdd() {
        const value = this.form.value['addOption'].trim();
        if (this.isUnique(value)) {
            (<FormArray>this.form.get('options')).push(new FormControl(value));
            this.form.patchValue({addOption: null});
        }
        else{
            this.form.get('addOption').setErrors({'isNotUnique':true});
        }
    }

    onRemove(index: number) {
        (<FormArray>this.form.get('options')).removeAt(index);
    }

    onReset() {
        this.form.reset({
            'question': null,
            'addOption': null,
        });

        (<FormArray>this.form.get('options')).clear();
        (<FormArray>this.form.get('options')).push(new FormControl('answer option 1'));
        (<FormArray>this.form.get('options')).push(new FormControl('answer option 2'));
        this.resetEvent.emit();
    }

    isUnique(value):boolean {
        return !this.form.value.options.find(item => item === value);
    }

    set subscribe(subscription: Subscription) {
        this.subscriptions.push(subscription);
    }

    get isQuestionValid(): boolean {
        return !this.form.get('question').dirty || this.form.get('question').valid;
    }

    get isAddOptionValid(): boolean {
        return this.form.get('addOption').valid;
    }

    get isNotUnique(): boolean {
        return this.form.get('addOption').errors && this.form.get('addOption').errors['isNotUnique'];
    }

    get optionsForm() {
        return (this.form.get('options') as FormArray).controls;
    }

    get showRemove(): boolean{
        return this.form.value.options.length > 2;
    }
}

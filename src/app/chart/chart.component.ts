import {Component, Input} from '@angular/core';
import {ChartDataSets, ChartOptions} from 'chart.js';
import {Color} from "ng2-charts";

@Component({
    selector: 'app-chart',
    templateUrl: './chart.component.html',
    styleUrls: ['./chart.component.scss'],
})
export class ChartComponent {
    @Input() question: string;
    @Input() labels: string[];
    @Input() data: ChartDataSets[];
    @Input() colors: Color[] = [];
    @Input() total: number;
    barChartOptions: ChartOptions = {
        responsive: true,
        scales: {
            xAxes: [{}], yAxes: [{
                ticks: {
                    min: 0, max: 60,
                }
            }]
        },
    };
}
